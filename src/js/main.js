"use strict";
// import { ItemDescription } from "./classItemDescription";
// import { SceneryItem } from "./classSceneryItem";
// import { Scenery } from "./classScenery";

// function setClue(listItems) {
//   for (let i = 0; i < listItems.length; i++) {
//     let item = listItems[i]["clue"];
//     if (item === true) {
//       return (item);
//     }
//   }
// }

let listItem = [];
let ch = "character";
let it = "item";
let pl = "place";

let description = new ItemDescription("description");
let description1 = new ItemDescription("pascal");
let description2 = new ItemDescription("eloise");

let pascal = new SceneryItem("pascal", ch, [description1, description, description], true); 
let eloise = new SceneryItem("eloise", ch, [description]);

let knife = new SceneryItem("couteau", it, [pascal, description2]);

let cityHall = new SceneryItem("mairie", pl, [eloise]);

listItem.push(cityHall);
listItem.push(pascal);
listItem.push(knife);
listItem.push(eloise);

let scene1 = new Scenery("meurtre à la mairie", listItem);

console.log(scene1);

let scenery = document.querySelector("#scenery");
let sectionDescription = document.querySelector("#description");

for (let i = 0; i < scene1["listItems"].length; i++) {
  let div = document.createElement("div");
  div.setAttribute("id", scene1["listItems"][i]["name"]);
  if (scene1['listItems'][i]['clue']) {
    div.setAttribute("class", `cell${i} clue scenery-item col-md-3`);
  } else {
    div.setAttribute("class", `cell${i} scenery-item col-md-3`);
    div.style.backgroundImage = scene1["listItems"][i]["image"];
  }
  div.style.height = "50vh";
  scenery.appendChild(div);
}

let cell0 = document.querySelector('.cell0');
cell0.addEventListener('click', function(){
  sectionDescription.innerHTML = "";
  if (scene1['listItems'][0]['clue']) {
    scene1['listItems'][0].getClue();
  } else {
    scene1['listItems'][0].getDescription("cell0");
  }
})

let cell1 = document.querySelector('.cell1');
cell1.addEventListener('click', function(){
  sectionDescription.innerHTML = "";
  scene1['listItems'][1].getDescription("cell1");
})

let cell2 = document.querySelector('.cell2');
cell2.addEventListener('click', function(){
  sectionDescription.innerHTML = "";
  scene1['listItems'][2].getDescription("cell2");
})

let cell3 = document.querySelector('.cell3');
cell3.addEventListener('click', function(){
  sectionDescription.innerHTML = "";
  scene1['listItems'][3].getDescription("cell3");
})