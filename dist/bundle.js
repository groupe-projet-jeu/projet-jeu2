/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/main.js":
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n// import { ItemDescription } from \"./classItemDescription\";\n// import { SceneryItem } from \"./classSceneryItem\";\n// import { Scenery } from \"./classScenery\";\n\n// function setClue(listItems) {\n//   for (let i = 0; i < listItems.length; i++) {\n//     let item = listItems[i][\"clue\"];\n//     if (item === true) {\n//       return (item);\n//     }\n//   }\n// }\n\nlet listItem = [];\nlet ch = \"character\";\nlet it = \"item\";\nlet pl = \"place\";\n\nlet description = new SceneryItem(\"description\");\n\nlet pascal = new SceneryItem(\"pascal\", [description, description, description], ch); \nlet eloise = new SceneryItem(\"eloise\", [description], ch, true);\n\nlet knife = new SceneryItem(\"couteau\",[description, description], it);\n\nlet cityHall = new SceneryItem(\"mairie\",[description], pl);\n\nlistItem.push(cityHall);\nlistItem.push(pascal);\nlistItem.push(knife);\nlistItem.push(eloise);\n\nlet scene1 = new Scenery(\"meurtre à la mairie\", listItem);\n\nconsole.log(scene1);\n\nlet scenery = document.querySelector(\"#scenery\");\nfor (let i = 0; i < scene1[\"listItems\"].length; i++) {\n  let div = document.createElement(\"div\");\n  div.setAttribute(\"id\", scene1[\"listItems\"][i][\"name\"]);\n  div.setAttribute(\"class\", \"scenery-item col-md-3\");\n  div.style.backgroundImage = scene1[\"listItems\"][i][\"image\"];\n  div.style.height = \"50vh\";\n  scenery.appendChild(div);\n}\n\n//# sourceURL=webpack:///./src/js/main.js?");

/***/ })

/******/ });